// import { AsyncStorage } from 'react-native';
import ApiUtils from '@lib/ApiUtils';
import { Actions } from 'react-native-router-flux';
/**
  * Get Login Credentials from AsyncStorage
  */
// async function getCredentialsFromStorage() {
//   const values = await AsyncStorage.getItem('api/credentials');
//   const jsonValues = JSON.parse(values);

//   // Return credentials from storage, or an empty object
//   if (jsonValues.email || jsonValues.password) return jsonValues;
//   return {};
// }

/**
  * Save Login Credentials to AsyncStorage
  */
// async function saveCredentialsToStorage(email = '', password = '') {
//   await AsyncStorage.setItem('api/credentials', JSON.stringify({ email, password }));
// }

/**
  * Remove Login Credentials from AsyncStorage
  */
// async function removeCredentialsFromStorage() {
//   await AsyncStorage.removeItem('api/credentials');
// }


export function login(username, password) {
  //console.log('log 4 {0}, {1}', username, password);
  return async (dispatch) => {
    const data = { username, password,
    };
    fetch('http://jira.bch.bancodechile.cl:8080/rest/auth/1/session', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })

      .then(ApiUtils.checkStatus)
      .then(response => response.json())
      .then((responseJson) => {
        //console.log(responseJson);
        Actions.tabBar();
        return dispatch({
          type: 'USER_LOGIN',
        });
      })
      .catch((error) => {
        //console.error(error);
      });
  };
}
