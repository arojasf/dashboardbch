// import { AsyncStorage } from 'react-native';
import ApiUtils from '@lib/ApiUtils';
import { Actions } from 'react-native-router-flux';
/**
  * Get Login Credentials from AsyncStorage
  */
// async function getCredentialsFromStorage() {
//   const values = await AsyncStorage.getItem('api/credentials');
//   const jsonValues = JSON.parse(values);

//   // Return credentials from storage, or an empty object
//   if (jsonValues.email || jsonValues.password) return jsonValues;
//   return {};
// }

/**
  * Save Login Credentials to AsyncStorage
  */
// async function saveCredentialsToStorage(email = '', password = '') {
//   await AsyncStorage.setItem('api/credentials', JSON.stringify({ email, password }));
// }

/**
  * Remove Login Credentials from AsyncStorage
  */
// async function removeCredentialsFromStorage() {
//   await AsyncStorage.removeItem('api/credentials');
// }


export function getStoryByRelease(currentReleaseKey) {
  return async (dispatch) => {
    fetch('http://jira.bch.bancodechile.cl:8080/sr/com.atlassian.jira.plugins.jira-importers-plugin:searchrequest-json/temp/SearchRequest.json?jqlQuery=category+%3D+%22Nueva+Internet%22+AND+issuetype+%3D+Story+AND+affectedVersion+%3D+' + currentReleaseKey, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })

      .then(ApiUtils.checkStatus)
      .then(response => response.json())
      .then((responseJson) => {
        //console.log('arturo 1', JSON.stringify(responseJson));
        return dispatch({
          type: 'JIRA_STORIES',
          data: responseJson,
        });
      })
      .catch((error) => {
        //console.error(error);
      });
  };
}
