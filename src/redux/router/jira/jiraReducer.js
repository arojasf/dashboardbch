
import Store from './store';

// Set initial state
export const initialState = Store;

export default function recipeReducer(state = initialState, action) {
  switch (action.type) {
    case 'JIRA_STORIES': {
      return {
        ...state,
        jira: action.data || {},
      };
    }
    default:
      return state;
  }
}
