
import { combineReducers } from 'redux';

// Our custom reducers
// We need to import each one here and add them to the combiner at the bottom
import router from '@redux/router/reducer';
import user from '@redux/router/user/userReducer';
import recipe from '@redux/router/jira/jiraReducer';
// Combine all
const appReducer = combineReducers({
  router,
  user,
  recipe,
});

// Setup root reducer
const rootReducer = (state, action) => {
  const newState = (action.type === 'RESET') ? undefined : state;
  return appReducer(newState, action);
};

export default rootReducer;
