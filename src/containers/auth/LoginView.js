import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Button,
} from 'react-native';

import Loading from '@ui/Loading';

// import { AppStyles } from '@theme/';
// import { Alerts, Card, Spacer, Text, Button } from '@ui/';
// import { Actions } from 'react-native-router-flux';

// let redirectTimeout;
class AuthForm extends Component {
  static propTypes = {
    submit: PropTypes.func.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {
      username: 'monitor',
      password: 'monitorS17',
      loading: false,
    };
  }
  componentDidMount = () => {
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
        loading: false,
    });
  }
  handleSubmit = () => {
    if (this.props.submit) {
      this.setState({
        loading: true,
      });
      // console.log('se eejcuto esta wea?, {0}', this.state);
      this.props.submit(this.state.username, this.state.password);
    }
  };
  render() {
    if (this.state.loading) return <Loading />;

    return (
      <ScrollView style={{ padding: 20 }}>
        <Text>
          {this.state.data}
        </Text>
        <Text style={{ fontSize: 27 }}>
          Login
                </Text>
        <TextInput
          placeholder="Username"
          autoCapitalize="none"
          autoCorrect={false}
          autoFocus={true}
          keyboardType="email-address"
          value={this.state.username}
          onChangeText={(text) => this.setState({ username: text })}
        />
        <TextInput
          placeholder="Password"
          autoCapitalize="none"
          autoCorrect={false}
          secureTextEntry={true}
          value={this.state.password}
          onChangeText={(text) => this.setState({ password: text })}
        />
        <View style={{ margin: 7 }} />
        <Button onPress={this.handleSubmit} title="Login" />
      </ScrollView>
    );
  }
}

/* Export Component ==================================================================== */
export default AuthForm;
