import { connect } from 'react-redux';

// Actions
import * as UserActions from '@redux/router/user/userAction';

// The component we're mapping to
import FormRender from './LoginView';

// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  username: state.username,
  password: state.password,
});
const mapDispatchToProps = {
  submit: UserActions.login,
};

export default connect(mapStateToProps, mapDispatchToProps)(FormRender);
