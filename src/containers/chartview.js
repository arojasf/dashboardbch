'use strict';
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Echarts from 'react-native-charting';

export default class ChartScreen extends Component {
    static navigationOptions = {
        title: 'Grafico',
    };
    constructor(props) {
    super(props);
  }
  render() {
    const option = {
      title: {
        text: 'ECharts demo'
      },
      tooltip: {},
      legend: {
        data:['销量']
      },
      xAxis: {
        data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
      },
      yAxis: {},
      series: [{
        name: '销量',
        type: 'bar',
        data: [5, 20, 36, 10, 10, 20]
      }]
    };
    return (
      <View>
        
        <Echarts option={option} height={300} />
        <Text
          onPress={() => Actions.modal()}
        >Open Modal
        </Text>
        </View>
    );
  }
}
