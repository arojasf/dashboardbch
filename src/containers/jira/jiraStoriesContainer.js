import { connect } from 'react-redux';

// Actions
import * as jiraAction from '@redux/router/jira/jiraAction';

// The component we're mapping to
import FormRender from './jiraStoriesView';

// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  currentReleaseKey: state.currentReleaseKey,
  releases: state.recipe.releases || [],
  jira: (state.recipe && state.recipe.jira) ? state.recipe.jira : null,
});
const mapDispatchToProps = {
  getStoryByRelease: jiraAction.getStoryByRelease,
};

export default connect(mapStateToProps, mapDispatchToProps)(FormRender);
