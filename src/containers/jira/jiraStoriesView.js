import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    StyleSheet,
    InteractionManager,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Echarts from 'react-native-charting';
import ModalPicker from 'react-native-modal-picker';

import PropTypes from 'prop-types';

import Loading from '@ui/Loading';

export default class ChartScreen extends Component {
    static propTypes = {
        releases: PropTypes.arrayOf(PropTypes.object).isRequired,
        getStoryByRelease: PropTypes.func.isRequired,
        jira: PropTypes.object.isRequired,
    }

    static defaultProps = {
        releases: [],
        jira: {},
    }
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            currentReleaseKey: '',
            recipes: {},
            option: {},
        };
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ loading: false });
        if (nextProps && nextProps.jira && nextProps.jira.projects) {
            const issues = [];
            nextProps.jira.projects.forEach(function (project) {
                const stories = project.issues.filter(x => x.issueType === 'Story');
                stories.forEach(function (story) {
                    issues.push(story);
                });
            });
            const avanceHistorias = [];

            issues.forEach(function (issue) {
                const progress = issue.customFieldValues.filter(x => x.fieldName === 'Progress (%)')[0] || null;
                
                const histAux = {
                    name: issue.summary,
                    avance: progress.value,
                    key: issue.key,
                    estado: issue.status,
                    scrum: issue.assignee
                };
                avanceHistorias.push(histAux);
            });
            this.handleOptionChart(avanceHistorias);
        }
    }
    handleOptionChart = (avanceHistorias) => {
        console.log('arturo: ', JSON.stringify(avanceHistorias));
        const option={
                title: {
                    text: 'Avance de Historias ' + this.state.currentReleaseKey,
                },
                tooltip: {},
                legend: {
                    data: ['arturo'],
                },
                xAxis: {
                    data: avanceHistorias.map(x => x.name),
                },
                yAxis: {},
                series: [{
                    name: '销量',
                    type: 'bar',
                    data: avanceHistorias.map(x => x.avance),
                }]
            };
        this.setState({ option: option });
    }
    handleRelease = (option) =>{
        this.setState({ currentReleaseKey: option.label });
        this.getStories();
    }
    getStories = () =>
    {
        this.setState({ loading: true });
        this.props.getStoryByRelease(this.state.currentReleaseKey);
    }
    componentDidMount = () => {
        InteractionManager.runAfterInteractions(() => {
            if (this.props.releases) {
                const timestamp = new Date().getTime() - 86400000;
                var list = this.props.releases;
                const releaseSelected = list.filter(x => x.timestamp >= timestamp)[0] || null;

                this.setState({ currentReleaseKey: releaseSelected.name });

                //console.log('arturo :', this.state.currentReleaseKey, releaseSelected.name);
                this.props.getStoryByRelease(releaseSelected.name);

                // list.forEach(function(x) {
                //     if(x.timestamp >= timestamp){
                //        console.log('arturo :', x.timestamp, timestamp, x.name );

                //     }
                //     //console.log('arturo :', x.timestamp, timestamp );
                // });
                //console.log('hola arturo: {0}, {1}, {2}',new Date().getTime(), timestamp, doubled);
                //const r = list.map(x => x.timestamp >= timestamp).FirstOrDefault();
                //

            }
        });

    }
    render() {
        const data = [];
        let idx = 0;
        this.props.releases.forEach((r) => {
            data.push({
                key: idx.toString(),
                id: r.$id.toString(),
                label: r.name,
            });
            idx += 1;
        });
        const modalStyles = StyleSheet.create({
            selectModalContainer: {
                position: 'relative',
                width: 200,
                height: 30,
                marginTop: 50,
            },
            selectModalComponent: {
                position: 'absolute',
                opacity: 0,
                width: 200,
                zIndex: 2,
            },
            inputStyleSelectModal: {
                position: 'absolute',
                textAlign: 'right',
                width: 200,
                height: 30,
                zIndex: 1
            }
        });
        if (this.state.loading) return <Loading />;

        return (
            
            <View>
                <View style={modalStyles.selectModalContainer}>
                    <ModalPicker
                        data={data}
                        style={modalStyles.selectModalComponent}
                        initValue="Selecciona el release"
                        onChange={(option) => { this.handleRelease(option); }}
                    />

                    <TextInput
                        style={modalStyles.inputStyleSelectModal}
                        editable={false}
                        placeholder="Selecciona el release"
                        value={this.state.currentReleaseKey}
                    />

                </View>
                {this.state.loading === false &&
                    <Echarts option={this.state.option} height={300} />
                }
            </View>
        );
    }
}
