/**
 * Tabs Scenes
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React from 'react';
import { Scene } from 'react-native-router-flux';

// Consts and Libs
import { AppConfig } from '@constants/';
import { AppStyles, AppSizes } from '@theme/';

// Components
import { TabIcon } from '@ui/';

// Scenes
import Placeholder from '@components/Placeholder';
import ChartScreen from '@containers/jira/jiraStoriesContainer';
import ListViewDemo from '@containers/list/listview';
// import Error from '@components/general/Error';
// import StyleGuide from '@containers/StyleGuideView';
// import Recipes from '@containers/recipes/Browse/BrowseContainer';
// import RecipeView from '@containers/recipes/RecipeView';

/* Routes ==================================================================== */
const scenes = (
  <Scene key={'tabBar'} tabs tabBarIconContainerStyle={AppStyles.tabbar} pressOpacity={0.95}>
    <Scene
      key={'chart'}
      hideNavBar={true} 
      title={'Chart'}
      component={ChartScreen}
      icon={props => TabIcon({ ...props, icon: 'search' })}
    >
    </Scene>
    <Scene
      key={'recipes2'}
      hideNavBar={true} 
      title={'Recipes2'}
      component={Placeholder}
      icon={props => TabIcon({ ...props, icon: 'search' })}
    >
    </Scene>
    <Scene
      key={'list'}
      hideNavBar={true} 
      title={'Basic list'}
      component={ListViewDemo}
      icon={props => TabIcon({ ...props, icon: 'search' })}
    >
    </Scene>

  </Scene>
);

export default scenes;
