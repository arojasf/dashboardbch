import React from 'react';
import { Actions, Scene } from 'react-native-router-flux';

import AuthForm from '@containers/auth/LoginContainer';
import ModalScreen from '@containers/modal/modalAjustes';

import TabsScenes from './tabs';

export default Actions.create(
  <Scene key={'root'}>
    <Scene
      hideNavBar
      key={'splash'}
      component={AuthForm}
    />
    {TabsScenes}
    <Scene
      key="modal"
      direction="vertical"
      component={ModalScreen}
      title="Modal"
      hideNavBar
    />
  </Scene>,
);
