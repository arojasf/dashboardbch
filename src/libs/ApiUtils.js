// ApiUtils.js
const ApiUtils = {  
  checkStatus: function(response) {
    if (response.status >= 200 && response.status < 300) {
      //console.log('ok');
      return response;
    } else {
      //console.log('error');
      const error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  },
};
export { ApiUtils as default };
